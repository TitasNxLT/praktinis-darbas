<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [ShopController::class, 'index']);
Route::get('/add-product', [ShopController::class, 'viewForm']);
Route::get('/add-product/fake', [ShopController::class, 'storeFakeProduct']);
Route::post('/store-product', [ShopController::class, 'storeProduct']);
Route::get('/product/{product}', [ShopController::class, 'viewProduct']);
Route::get('/product/{product}/edit', [ShopController::class, 'viewEditForm']);
Route::patch('/product/{product}/edit', [ShopController::class, 'editProduct']);
Route::get('/product/{product}/remove', [ShopController::class, 'viewRemoveForm']);
Route::post('/product/{product}/remove', [ShopController::class, 'removeProduct']);

Route::get('/category/{category}', [ShopController::class, 'viewProductsByCategory']);
Route::get('/category/{category}/edit', [CategoryController::class, 'viewEditForm']);
Route::patch('/category/{category}/edit', [CategoryController::class, 'editCategory']);
Route::get('/category/{category}/remove', [CategoryController::class, 'viewRemoveForm']);
Route::post('/category/{category}/remove', [CategoryController::class, 'removeCategory']);

Route::get('/dashboard', [ShopController::class, 'viewAdminPanel'])->name('dashboard');
Route::get('/dashboard/products', [ShopController::class, 'viewProductsDashboard'])->name('products');
Route::get('/dashboard/categories', [CategoryController::class, 'index'])->name('categories');

Route::get('/add-category', [CategoryController::class, 'viewForm']);
Route::get('/add-category/fake', [CategoryController::class, 'storeFakeCategory']);
Route::post('/store-category', [CategoryController::class, 'storeCategory']);

require __DIR__.'/auth.php';
