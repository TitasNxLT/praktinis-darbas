<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Categories') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <p class="text-2xl">{{ __('Categories') }}</p>
                    <table class="w-full table-fixed border-colapse border-2 border-primary-800">
                    <thead>
                        <tr class="bg-primary-200">
                        <th class="w-1/6 border-2 border-primary-600">{{ __('Name') }}</th>
                        <th class="w-1/12 border-2 border-primary-600">{{ __('Actions') }}<a href="/add-category">➕</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                        <tr class="hover:bg-primary-100">
                        <td class="p-1 border border-primary-600">{{ $category->name }}</td>
                        <td class="p-1 border border-primary-600"><a href="/category/{{ $category->id }}">👁</a> <a href="/category/{{ $category->id }}/edit">✏</a> <a href="/category/{{ $category->id }}/remove">❌</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
