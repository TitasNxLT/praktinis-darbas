<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <p class="text-2xl">{{ __('Your products') }}</p>
                    <table class="w-full table-fixed border-colapse border-2 border-primary-800">
                    <thead>
                        <tr class="bg-primary-200">
                        <th class="w-1/6 border-2 border-primary-600">{{ __('Name') }}</th>
                        <th class="w-4/6 border-2 border-primary-600">{{ __('Description') }}</th>
                        <th class="w-1/12 border-2 border-primary-600">{{ __('Price') }}</th>
                        <th class="w-1/12 border-2 border-primary-600">{{ __('Actions') }}<a href="/add-product">➕</a></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($userProducts as $userProduct)
                        <tr class="hover:bg-primary-100">
                        <td class="p-1 border border-primary-600">{{ $userProduct->name }}</td>
                        <td class="p-1 border border-primary-600">{{ Str::limit($userProduct->description, 50) }}</td>
                        <td class="p-1 border border-primary-600">{{ number_format($userProduct->price, 2) }}€</td>
                        <td class="p-1 border border-primary-600"><a href="/product/{{ $userProduct->id }}">👁</a> <a href="/product/{{ $userProduct->id }}/edit">✏</a> <a href="/product/{{ $userProduct->id }}/remove">❌</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
