@extends('main')
@section('content')
<div class="relative bg-white dark:bg-gray-900 overflow-hidden">
  <div class="max-w-7xl mx-auto">
    <div class="relative z-10 pb-8 bg-white dark:bg-gray-800 sm:pb-16 md:pb-20 lg:max-w-2xl lg:w-full lg:pb-28 xl:pb-32">
      <svg class="hidden lg:block absolute right-0 inset-y-0 h-full w-48 text-white dark:text-gray-700 transform translate-x-1/2" fill="currentColor" viewBox="0 0 100 100" preserveAspectRatio="none" aria-hidden="true">
        <polygon points="50,0 100,0 50,100 0,100" />
      </svg>

      <main class="mt-10 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
        <div class="sm:text-center lg:text-left">
          <h1 class="text-2xl tracking-tight font-extrabold text-primary-700 sm:text-5xl md:text-6xl">
            <span class="block xl:inline">{{$category->name}}</span>
            <span class="block text-primary-500 xl:inline">Skateboards</span>
          </h1>
        </div>
      </main>
    </div>
  </div>
  <div class="lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2">
    <img class="h-56 w-full object-cover sm:h-72 md:h-96 lg:w-full lg:h-full" src="https://images.unsplash.com/photo-1551434678-e076c223a692?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2850&q=80" alt="">
  </div>
</div>
<div class="bg-white dark:bg-gray-800 md:container md:mx-auto pt-5">
{{-- TODO: Pakeisti justify-items-center i kazka kita --}}
  <div class="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-5">
    @foreach ($products as $product)
      <div class="bg-gray-200 dark:bg-gray-700 max-w-xs rounded overflow-hidden shadow-md hover:shadow-2xl">
        <img class="w-full" src="{{ url('storage/'.$product->img) }}" alt="Product photo">
        <div class="px-6 py-4">
          <div class="font-bold text-xl mb-2"><a href="/product/{{ $product->id }}">{{ $product->name }}</a></div>
          <p class="text-gray-700 dark:text-gray-500 text-base break-all">
            {{ Str::limit($product->description, 50) }}
            <p>Price: {{ number_format($product->price, 2) }} €</p>
          </p>
        </div>
        <div class="px-6 pt-4 pb-2">
          @foreach (json_decode($product->categories) as $category)
            <a class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2" href="/category/{{ $category }}">#{{ App\Models\Category::where('id', $category)->get()[0]->name }}</a>
          @endforeach
        </div>
      </div>
    @endforeach
  </div>
    @if(count($products) == 0)
      <div class="p-5 font-bold text-center text-gray-700 dark:text-gray-200 bg-gray-200 dark:bg-gray-700 rounded overflow-hidden shadow-md hover:shadow-2xl">
      We couldn't find any products with this category
      </div>
    @endif
</div>
@endsection
