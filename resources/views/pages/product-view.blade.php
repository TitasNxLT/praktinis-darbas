@extends('main')
@section('content')

<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

<div class="bg-white dark:bg-gray-800">
    <main class="my-8">
        <div class="container mx-auto px-6">
            <div class="md:flex md:items-center">
                <div class="w-full h-64 md:w-1/2 lg:h-96">
                    <img class="h-full w-full rounded-md object-cover max-w-lg mx-auto" src="{{ url('storage/'.$product->img) }}" alt="{{ $product->name }}">
                </div>
                <div class="w-full max-w-lg mx-auto mt-5 md:ml-8 md:mt-0 md:w-1/2">
                    <h3 class="text-gray-700 dark:text-gray-300 uppercase text-lg">{{ $product->name }}</h3>
                    <h3 class="text-gray-500 dark:text-gray-500 text-lg">{{ $product->description }}</h3>
                    <div class="pt-1 pb-2">
                    @foreach (json_decode($product->categories) as $category)
                        <a class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2" href="/category/{{ $category }}">#{{ App\Models\Category::where('id', $category)->get()[0]->name }}</a>
                    @endforeach
                    </div>
                    <span class="text-gray-500 dark:text-gray-100 mt-3">{{ number_format($product->price, 2) }} €</span>
                    <hr class="my-3">
                    <div class="flex items-center mt-6">
                        <button class="px-8 py-2 bg-indigo-600 text-white text-sm font-medium rounded hover:bg-indigo-500 focus:outline-none focus:bg-indigo-500">Order Now</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>



@endsection