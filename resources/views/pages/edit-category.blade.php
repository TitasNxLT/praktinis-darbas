@extends('main')
@section('content')
        <form method="POST" class="mt-5 w-95 mx-5" enctype="multipart/form-data">
            @include('_partials.errors')
            {{ method_field("PATCH") }}
            @csrf
              <div class="shadow sm:rounded-md sm:overflow-hidden">
                <div class="px-4 py-5 bg-white dark:bg-gray-600 space-y-6 sm:p-6">
                <p class="text-3xl text-center text-gray-700 dark:text-gray-50">Category editing</p>

                <div class="col-span-6 sm:col-span-3">
                    <label for="name" class="block text-sm font-medium text-gray-700 dark:text-gray-50">Name</label>
                    <input type="text" name="name" id="name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{$category->name}}">
                </div>

                <div class="px-4 py-3 bg-gray-50 dark:bg-gray-500 text-right sm:px-6 flex justify-center">
                    <button type="submit"
                        class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Edit
                    </button>
                </div>
            </div>
        </form>

  <script src="{{ asset('js/dragndrop.js') }}"></script>
@endsection