@extends('main')
@section('content')
<form action="store-product" method="POST" class="mt-5 w-95 mx-5" enctype="multipart/form-data">
    @include('_partials.errors')
    @csrf
    <div class="shadow sm:rounded-md sm:overflow-hidden">
        <div class="px-4 py-5 bg-white dark:bg-gray-600 space-y-6 sm:p-6">
            <p class="text-3xl text-center text-gray-700 dark:text-gray-50">Add product</p>

            <div class="col-span-6 sm:col-span-3">
                <label for="name" class="block text-sm font-medium text-gray-700 dark:text-gray-50">Name</label>
                <input type="text" name="name" id="name"
                    class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md"
                    value="{{old('name')}}">
            </div>

            <div class="grid grid-cols-3 gap-6">
                <div class="col-span-3 sm:col-span-2">
                    <label for="price" class="block text-sm font-medium text-gray-700 dark:text-gray-50">
                        Price
                    </label>
                    <div class="mt-1 flex rounded-md shadow-sm w-20">
                        <input type="number" name="price" id="price" step="0.01" value="{{old('price')}}"
                            class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-50 rounded-none rounded-l-md sm:text-sm border-gray-300">
                        <span
                            class="inline-flex items-center px-3 rounded-r-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                            €
                        </span>
                    </div>
                </div>
            </div>

            <div>
                <label for="description" class="block text-sm font-medium text-gray-700 dark:text-gray-50">
                    Description
                </label>
                <div class="mt-1">
                    <textarea id="description" name="description" rows="3"
                        class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md">{{old('description')}}</textarea>
                </div>
                <p class="mt-2 text-sm text-gray-500 dark:text-gray-300">
                    Brief description for your product.
                </p>
            </div>

            <div class="col-span-6 sm:col-span-3">
                <legend class="text-base font-medium text-gray-700 dark:text-gray-50">Categories</legend>
                <div class="mt-4 space-y-4">
                    <div class="flex items-start">
                        @foreach (App\Models\Category::all() as $category)
                        <div class="flex items-center h-5">
                            <input id="category[{{ $category->id }}]" name="category[{{ $category->id }}]"
                                type="checkbox"
                                class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                        </div>
                        <div class="ml-2 text-sm mr-3">
                            <label for="category[{{ $category->id }}]"
                                class="font-medium text-gray-700 dark:text-gray-50">{{ $category->name }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div>
                <label class="block text-sm font-medium text-gray-700 dark:text-gray-50">
                    Image
                </label>
                <div class="mt-1 flex items-center">
                    <input id="img" name="img" type="file">
                </div>
            </div>

            <div class="px-4 py-3 bg-gray-50 dark:bg-gray-500 text-right sm:px-6 flex justify-center">
                <button type="submit"
                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Create
                </button>
                <a href="/add-product/fake"
                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Create with fake data
                </a>
            </div>
        </div>
</form>

@endsection
