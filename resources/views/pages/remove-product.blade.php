@extends('main')
@section('content')
        <form method="POST" class="mt-5 w-95 mx-5" enctype="multipart/form-data">
            @include('_partials.errors')
            @csrf
              <div class="shadow sm:rounded-md sm:overflow-hidden">
                <div class="px-4 py-5 bg-white dark:bg-gray-600 space-y-6 sm:p-6">
                <p class="text-3xl text-center text-gray-700 dark:text-gray-50">Product removal</p>

                <div class="col-span-6 sm:col-span-3">
                    <label for="name" class="block text-sm font-medium text-gray-700 dark:text-gray-50">Name</label>
                    <input readonly disabled type="text" name="name" id="name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" value="{{$product->name}}">
                </div>

                <div class="grid grid-cols-3 gap-6">
                    <div class="col-span-3 sm:col-span-2">
                    <label for="price" class="block text-sm font-medium text-gray-700 dark:text-gray-50">
                        Price
                    </label>
                    <div class="mt-1 flex rounded-md shadow-sm w-20">
                        <input readonly disabled type="number" name="price" id="price" step="0.01" value="{{$product->price}}"
                        class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-50 rounded-none rounded-l-md sm:text-sm border-gray-300">
                        <span
                        class="inline-flex items-center px-3 rounded-r-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                        €
                        </span>
                    </div>
                    </div>
                </div>

                <div>
                    <label for="about" class="block text-sm font-medium text-gray-700 dark:text-gray-50">
                    Description
                    </label>
                    <div class="mt-1">
                    <textarea readonly disabled id="about" name="about" rows="3"
                        class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md">{{$product->description}}</textarea>
                    </div>
                    <p class="mt-2 text-sm text-gray-500 dark:text-gray-300">
                    Brief description for your product.
                    </p>
                </div>

                <div class="col-span-6 sm:col-span-3">
                    <legend class="text-base font-medium text-gray-700 dark:text-gray-50">Categories</legend>
                    <div class="mt-4 space-y-4">
                        <div class="flex items-start">
                            @foreach (App\Models\Category::all() as $category)
                                <?php
                                $categoryMatch = false;
                                foreach (json_decode($product->categories) as $category2) {
                                    if($category2 == $category->id) $categoryMatch = true;
                                }
                                ?>
                                @if($categoryMatch)
                                    <div class="flex items-center h-5">
                                        <input id="category[{{ $category->id }}]" name="category[{{ $category->id }}]" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded" checked>
                                    </div>
                                @else
                                    <div class="flex items-center h-5">
                                        <input id="category[{{ $category->id }}]" name="category[{{ $category->id }}]" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                @endif
                                <div class="ml-2 text-sm mr-3">
                                    <label for="category[{{ $category->id }}]" class="font-medium text-gray-700 dark:text-gray-50">{{ $category->name }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div>
                    <label class="block text-sm font-medium text-gray-700 dark:text-gray-50">
                        Image
                    </label>
                    <div class="w-full h-64 md:w-1/2 lg:h-96">
                        <img class="h-full w-full rounded-md object-cover max-w-lg mx-auto" src="{{ url('storage/'.$product->img) }}" alt="{{ $product->name }}">
                    </div>
                </div>

                <div class="px-4 py-3 bg-gray-50 dark:bg-gray-500 text-right sm:px-6 flex justify-center">
                <button type="submit"
                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-800 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-400">
                    Remove
                </button>
                </div>
            </div>
        </form>

  <script src="{{ asset('js/dragndrop.js') }}"></script>
@endsection