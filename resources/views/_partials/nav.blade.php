<nav class="flex items-center justify-between flex-wrap bg-black p-6" x-data="{ open: true }">
  <div class="flex items-center flex-shrink-0 text-primary-200 mr-6">
    <span class="font-semibold text-xl tracking-tight">Riedlentės</span>
  </div>
  <div class="block lg:hidden">
    <button @click="open = ! open" class="flex items-center px-3 py-2 border rounded text-primary-200 border-primary-400 hover:text-primary-50 hover:border-primary-50">
      <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
    </button>
  </div>
  <div x-show="open" class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
    <div class="text-sm lg:flex-grow">
      <a href="/" class="block mt-4 lg:inline-block lg:mt-0 text-primary-200 hover:text-primary-50 mr-4">
        Parduotuvė
      </a>
    </div>
    @guest
    <div>
      <a href="/login" class="inline-block text-sm px-4 py-2 leading-none border rounded text-primary-200 border-white hover:border-transparent hover:text-primary-900 hover:bg-white lg:mx-2 mt-4 lg:mt-0">Login</a>
    </div>
    <div>
      <a href="/register" class="inline-block text-sm px-4 py-2 leading-none border rounded text-primary-200 border-white hover:border-transparent hover:text-primary-900 hover:bg-white lg:mx-2 mt-4 lg:mt-0">Register</a>
    </div>
    @endguest
    @auth
    <div>
      <a href="/dashboard" class="inline-block text-sm px-4 py-2 leading-none border rounded text-primary-200 border-white hover:border-transparent hover:text-primary-900 hover:bg-white lg:mx-2 mt-4 lg:mt-0">{{ Auth::user()->name }}</a>
    </div>
    @endauth
  </div>
</nav>