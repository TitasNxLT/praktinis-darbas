const dropContainer = document.querySelector('#dropContainer');
const fileInput = document.querySelector('#fileInput');

dropContainer.ondragover = dropContainer.ondragenter = function(event) {
  event.preventDefault();
}

dropContainer.ondrop = function(event) {
  // pretty simple -- but not for IE :(
  fileInput.files = event.dataTransfer.files;

  // If you want to use some of the dropped files
  const dT = new DataTransfer();
  dT.items.add(event.dataTransfer.files[0]);
  // dT.items.add(event.dataTransfer.files[3]);
  fileInput.files = dT.files;

  event.preventDefault();
};