<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Faker;

class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $categories = Category::paginate(10);
        return view('categories', compact('categories'));
    }

    public function viewForm() {
        return view("pages.add-category");
    }
    public function storeFakeCategory(){
        $faker = Faker\Factory::create();
        Category::create([
            'name' => $faker->colorName()
        ]);
        return redirect(route('categories'));
    }
    public function storeCategory(Request $request){

        $validated = $request->validate([
            'name' => 'required|max:255'
        ]);
        Category::create([
            'name' => request('name')
        ]);
        return redirect(route('categories'));
    }
    public function viewEditForm(Category $category) {
        return view('pages.edit-category', compact('category'));
    }
    public function editCategory(Request $request, Category $category) {
        $validated = $request->validate([
            'name' => 'required|max:255',
        ]);
        Category::where('id', $category->id)->update($request->only(['name']));

        return redirect(route('categories'));
    }

    public function viewRemoveForm(Category $category) {
        return view('pages.remove-category', compact('category'));
    }
    public function removeCategory(Category $category) {
        $products = Product::all();

        foreach ($products as $product) {
            $productCategories = json_decode($product->categories);
            for ($i=0; $i < count($productCategories); $i++) { 
                if ($productCategories[$i] === $category->id) {
                    unset($productCategories[$i]);
                    if(count($productCategories) == 0) array_push($productCategories, Category::all()[0]->id);
                    Product::where('id', $product->id)->update(['categories' => $productCategories]);
                }
            }
        }


        $category->delete();
        return redirect(route('categories'));
    }
}
