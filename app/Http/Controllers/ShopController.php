<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Storage;
use Faker;

class ShopController extends Controller
{
    public function __construct(){
        $this->middleware('auth', ['except'=>['index', 'viewProduct', 'viewProductsByCategory']]);
    }

    public function index(){
        $products = Product::paginate(10);
        return view('pages.home', compact('products'));
    }

    public function viewForm() {
        return view("pages.add-product");
    }
    public function storeFakeProduct()
    {
        $faker = Faker\Factory::create();
        $path = getcwd(); // /public
        $path = str_replace('public', 'storage/app/public/images', $path);
        $path_to_img = $faker->image($path);

        $filename = str_replace($path, 'images', $path_to_img);

        $categories = Category::all();
        $categoriesList = '[';
        foreach ($categories as $category) {
            if (rand(1, 10) < 5) $categoriesList .= $category->id . ', ';
        }
        $categoriesList = substr($categoriesList, 0, -2) . ']';
        if(strlen($categoriesList)<3) $categoriesList = '['. $categories[0]->id .']';

        Product::create([
            'name' => $faker->company(),
            'description' => $faker->text(100),
            'price' => $faker->numberBetween(1, 1000),
            'categories' => $categoriesList,
            'img' => $filename,
            'owner' => Auth::id()
        ]);
        return redirect('/');
    }
    public function storeProduct(Request $request){
        $validated = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'price' => 'required',
            'category' => 'required',
            'img' => 'required|mimes:jpg,jpeg,png|max:10000'
        ]);
        $path_to_img = $request->file('img')->store('public/images');
        $filename = str_replace('public/', '', $path_to_img);
        $categories = '[';
        foreach (request('category') as $category => $value) {
            $categories .= $category . ', ';
        }
        $categories = substr($categories, 0, -2) . ']';
        Product::create([
            'name' => request('name'),
            'description' => request('description'),
            'price' => request('price'),
            'categories' => $categories,
            'img' => $filename,
            'owner' => Auth::id()
        ]);

        return redirect('/');
    }

    public function viewProduct(Product $product) {
        return view('pages.product-view', compact('product'));
    }

    public function viewEditForm(Product $product) {
        if (Gate::allows('edit', $product)) {
            return view('pages.edit-product', compact('product'));
        }
        $error = ['code' => '403', 'message' => 'You don\'t have permission for this product'];
        return view('pages.error', compact('error'));
    }
    public function editProduct(Request $request, Product $product) {
        if (Gate::allows('edit', $product)) {
            $validated = $request->validate([
                'name' => 'required|max:255',
                'description' => 'required',
                'price' => 'required',
                'category' => 'required'
            ]);

            $categories = '[';
            foreach (request('category') as $category => $value) {
                $categories .= $category . ', ';
            }
            $categories = substr($categories, 0, -2) . ']';

            Product::where('id', $product->id)->update($request->only(['name', 'description', 'price']));
            Product::where('id', $product->id)->update(['categories' => $categories]);

            if($request->file('img') != null) {
                Storage::delete('public/'.$product->img);
                $path_to_img = $request->file('img')->store('public/images');
                $filename = str_replace('public/', '', $path_to_img);
                Product::where('id', $product->id)->update(['img' => $filename]);
            }
            return redirect('/product/'.$product->id);
        }
        $error = ['code' => '403', 'message' => 'You don\'t have permission for this product'];
        return view('pages.error', compact('error'));
    }

    public function viewRemoveForm(Product $product) {
        if (Gate::allows('delete', $product)) {
            return view('pages.remove-product', compact('product'));
        }
        $error = ['code' => '403', 'message' => 'You don\'t have permission for this product'];
        return view('pages.error', compact('error'));
    }
    public function removeProduct(Product $product) {
        if (Gate::allows('delete', $product)) {
            Storage::delete('public/'.$product->img);
            $product->delete();
            return redirect(route('dashboard'));
        }
        $error = ['code' => '403', 'message' => 'You don\'t have permission for this product'];
        return view('pages.error', compact('error'));
    }

    public function viewProductsByCategory(Category $category) {
        $allProducts = Product::all();
        $products = [];
        foreach ($allProducts as $product) {
            $productCategories = json_decode($product->categories);
            foreach ($productCategories as $productCategory) {
                if ($productCategory === $category->id) array_push($products, $product);
            }
        }
        return view('pages.products-by-category', compact('products', 'category'));
    }

    public function viewAdminPanel() {
        return view('dashboard');
    }
    public function viewProductsDashboard() {
        $userProducts = Product::where('owner', Auth::id())->get();
        return view('products', compact('userProducts'));
    }
}
